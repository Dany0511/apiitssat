const express = require('express');
const fs = require('fs');
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));
const port = 3000;
const manejaGet = (req, res) =>{
    console.log();("estoy en la pagina home");
    res.json({saludo:"hola mundo"});
   
}
const nombre = (req, res) =>{
    console.log("ejercicio");
    res.json({saludo:"carlos daniel mendoza fernandez"});
}
app.get('/home', manejaGet);
app.get('/CDMF', nombre);

const readFile = (err, data) => {
    if(err) console.log('Hubo un error');
    let info = JSON.parse(data);
    console.log(info);
    return info;
}

app.get('/people', (req, res) => {
    let data = fs.readFileSync('database/table.json');
    let info = JSON.parse(data);
    console.log('Leimos el archivo');
    console.log(info);
    res.json({response: info}) 
})
app.get('/people/:id', (req, res) =>{
  
    let identifier = req.params.id;
   
    let data = fs.readFileSync('database/table.json');
    let obJSON = JSON.parse(data);

    let response = null;
    for(let i =0; i < obJSON.length; i++){
        if (obJSON[i].id == identifier){
            response= obJSON[i];
            break;
        }
    }
    
    if(response == null){
    res.status(404).send();
    return ;
    }
    res.json(response);
})
app.post('/people', (req, res) => {
    console.log(req.body);
    let person = req.body;
    let data = fs.readFileSync('database/table.json');
    let info = JSON.parse(data);
    //validacion si el usuario ya existe en nuestra base de datos y si es asi entonces marca un mensaje que ya existe
    for(let k =0; k < info.length; k++){
        if (info [k].id == person.id){
            res.status(400).json({mensaje: 'El ususario ya existe'}); //mensaje
            return ;
        }
    }
    info.push(person);
    fs.writeFileSync('database/table.json', JSON.stringify(info));
    res.status(201).json(person);
})
app.listen(port, () => {
    console.log("El servidor esta escuchando en la url http://localhost:",port);
})      