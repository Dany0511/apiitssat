# Proyecto

Proyecto realizado en curso para ITSSAT "Programacion JS con Node"

## Autor

* Carlos Daniel Mendoza Fernandez

## Instrucciones

* Una vez que se descarga el repositorio, ejecutamos el comando ´npm install´
* Para ejecutar el servidor utilizando el comando ´node index.js´
* Es un cliente REST (Postman, Insomnia)
saludo